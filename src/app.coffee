restify = require 'restify'
redis = require 'redis-url'

if url = process.env.REDISTOGO_URL
	console.log 'Connecting to RedisToGo', url
	global.store = redis.connect(url)
else
	global.store = redis.createClient()


global.server = restify.createServer()

console.log 'Listening on', process.env.PORT or 7464
server.listen process.env.PORT or 7464 # p i n g

# handle errors that are produced by Exceptions.
# this makes it easier to produce errors in routes.
server.on 'uncaughtException', (req, res, route, err) ->
	if err.stack and err.name not in ['ValidatorError']
		console.error err.stack
	res.send 500, {
		status: "error",
		statusText: err.message or err
	}
server.on 'NotFound', (req, res, next) -> res.send 404, status: 'error', statusText: 'Endpoint not found'
server.on 'MethodNotAllowed', (req, res, next) -> res.send 404, status: 'error', statusText: 'Endpoint not found'
server.on 'VersionNotAllowed', (req, res, next) -> res.send 404, status: 'error', statusText: 'Endpoint not found'

# parse the query string and JSON-body automatically
server.use restify.queryParser()
server.use (req, res, next) -> next null, req.headers['content-type'] = 'application/json'
server.use restify.bodyParser mapParams: false

require('./routes')()
require('./worker')()
