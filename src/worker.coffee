async = require 'async'
restify = require 'restify'
module.exports = () ->
	exec = () ->

		time = Math.ceil 0.001 * new Date

		store.zrangebyscore 'request-queue', 0, time, (err, items) ->
			iterator = (id, next) ->
				store.get 'request:' + id, (err, item) ->
					if err or not item
						return next err or item

					try
						item = JSON.parse item
						if not item?.url
							throw true
					catch e
						zrem 'request-queue', id
						return next null


					# get the domain and path from the url
					[url, domain, path] = item.url.match /^(https?:\/\/[^\/]+)(\/.+)?/

					options = url: domain, connectTimeout: 5
					for k in ['accept', 'headers', 'userAgent'] when val = item[k]
						options[k] = val

					client = restify.createStringClient options
					method = item.method.toLowerCase()
					fn = (cb) -> client[method] path, item.body, cb
					if method in ['get', 'head']
						fn = (cb) -> client[method] path, cb

					fn (err, req, res, obj) ->
						res = res or {header: null, statusCode: 500, body: null}
						item.status = 'complete'
						item.response =
							headers: res.headers,
							statusCode: res.statusCode,
							body: res.body

						store.set 'request:' + id, JSON.stringify(item), (err) ->
							if err then return next err
							store.expire 'request:' + id, 3600
							store.zrem 'request-queue', id, (err) ->
								if err then return next err
								next()

			start = new Date
			async.each items, iterator, (err) ->
				if err then throw err

				if items.length > 0
					end = new Date
					time = 0.001 * (end - start)
					console.log "Processed #{items.length} requests in #{time} seconds"

				setTimeout exec, 1000

	exec()
