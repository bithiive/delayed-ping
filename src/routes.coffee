module.exports = () ->
	validate_item = (item) ->
		if not item.delay? and not item.time
			throw 'Either a delay (in seconds) or a time (in seconds since UNIX epoch) are required.'

		if not item.method or not (item.method.toString().toUpperCase() in ['GET', 'PUT', 'POST', 'DELETE'])
			throw 'A method must be present and a known HTTP verb'

		if not item.url or not item.url.toString().match /^https?:\/\//i
			throw 'A valid HTTP or HTTPS url must be present'

		if item.delay? and not item.time
			item.time = Math.round(0.001 * new Date) + (item.delay)
			delete item.delay

		if isNaN(item.time) or (item.time * 1000 < new Date)
			throw 'The time must be a number (seconds since UNIX epoch) and in the future. Current time: ' + Math.round 0.001 * new Date

		return item

	do404 = (res) ->
		res.send 404, {
			status: 'error',
			statusText: 'No queued item with that ID was found'
		}

	server.get '/', (req, res, next) ->
		res.send {
			status: 'Welcome!',
			statusText: 'This is a simple API for delaying an HTTP call until a set time.',
			documentation: 'http://docs.delayedping.apiary.io/',
			source: 'https://bitbucket.org/bithiive/delayed-ping',
			free: true,
			developer: 'Richard Lyon <richthegeek@gmail.com>'
		}

	server.post '/queue', (req, res, next) ->
		# verify we have the right fields.
		item = validate_item req.body
		# hash it to generate an ID, slap a pending status on there, and chuck it into redis
		item.id = require('crypto').createHash('sha1').update(JSON.stringify(item)).digest('hex')
		item.status = 'pending'
		item.response = null

		store.zadd 'request-queue', item.time, item.id, (err) ->
			if err then throw err
			store.set 'request:' + item.id, JSON.stringify(item), (err) ->
				if err then throw err
				res.send {
					status: 'ok',
					statusText: 'The item has been queued for ' + new Date(item.time * 1000),
					item: item
				}

	server.get '/queue/:id', (req, res, next) ->
		store.get 'request:' + req.params.id, (err, item) ->
			if item
				res.send {item: JSON.parse item}
			else
				do404 res

	server.put '/queue/:id', (req, res, next) ->
		server.get 'request:' + req.params.id, (err, original) ->
			if err then throw err

			if original
				item = validate_item req.body
				if original.time != item.time
					store.zincrby 'request-queue', item.time - original.time, (err) ->
						if err then throw err

				store.set 'request:' + item.id, JSON.stringify(item), (err) ->
					if err then throw err
					res.send {
						status: 'ok',
						statusText: 'The item has been queued for ' + new Date(item.time * 1000),
						item: item
					}
			else
				do404 res

	server.del '/queue/:id', (req, res, next) ->
		store.get 'request:' + req.params.id, (err, item) ->
			if err then throw err

			if item
				store.zrem 'request-queue', req.params.id, (err) ->
					if err then throw err
					store.del 'request:' + req.params.id, (err) ->
						if err then throw err

						res.send {
							status: 'ok',
							statusText: 'The item has been removed from the queue'
						}
			else
				do404 res
