// Generated by CoffeeScript 1.6.3
(function() {
  module.exports = function() {
    var do404, validate_item;
    validate_item = function(item) {
      var _ref;
      if ((item.delay == null) && !item.time) {
        throw 'Either a delay (in seconds) or a time (in seconds since UNIX epoch) are required.';
      }
      if (!item.method || !((_ref = item.method.toString().toUpperCase()) === 'GET' || _ref === 'PUT' || _ref === 'POST' || _ref === 'DELETE')) {
        throw 'A method must be present and a known HTTP verb';
      }
      if (!item.url || !item.url.toString().match(/^https?:\/\//i)) {
        throw 'A valid HTTP or HTTPS url must be present';
      }
      if ((item.delay != null) && !item.time) {
        item.time = Math.round(0.001 * new Date) + item.delay;
        delete item.delay;
      }
      if (isNaN(item.time) || (item.time * 1000 < new Date)) {
        throw 'The time must be a number (seconds since UNIX epoch) and in the future. Current time: ' + Math.round(0.001 * new Date);
      }
      return item;
    };
    do404 = function(res) {
      return res.send(404, {
        status: 'error',
        statusText: 'No queued item with that ID was found'
      });
    };
    server.get('/', function(req, res, next) {
      return res.send({
        status: 'Welcome!',
        statusText: 'This is a simple API for delaying an HTTP call until a set time.',
        documentation: 'http://docs.delayedping.apiary.io/',
        source: 'https://bitbucket.org/bithiive/delayed-ping',
        free: true,
        developer: 'Richard Lyon <richthegeek@gmail.com>'
      });
    });
    server.post('/queue', function(req, res, next) {
      var item;
      item = validate_item(req.body);
      item.id = require('crypto').createHash('sha1').update(JSON.stringify(item)).digest('hex');
      item.status = 'pending';
      item.response = null;
      return store.zadd('request-queue', item.time, item.id, function(err) {
        if (err) {
          throw err;
        }
        return store.set('request:' + item.id, JSON.stringify(item), function(err) {
          if (err) {
            throw err;
          }
          return res.send({
            status: 'ok',
            statusText: 'The item has been queued for ' + new Date(item.time * 1000),
            item: item
          });
        });
      });
    });
    server.get('/queue/:id', function(req, res, next) {
      return store.get('request:' + req.params.id, function(err, item) {
        if (item) {
          return res.send({
            item: JSON.parse(item)
          });
        } else {
          return do404(res);
        }
      });
    });
    server.put('/queue/:id', function(req, res, next) {
      return server.get('request:' + req.params.id, function(err, original) {
        var item;
        if (err) {
          throw err;
        }
        if (original) {
          item = validate_item(req.body);
          if (original.time !== item.time) {
            store.zincrby('request-queue', item.time - original.time, function(err) {
              if (err) {
                throw err;
              }
            });
          }
          return store.set('request:' + item.id, JSON.stringify(item), function(err) {
            if (err) {
              throw err;
            }
            return res.send({
              status: 'ok',
              statusText: 'The item has been queued for ' + new Date(item.time * 1000),
              item: item
            });
          });
        } else {
          return do404(res);
        }
      });
    });
    return server.del('/queue/:id', function(req, res, next) {
      return store.get('request:' + req.params.id, function(err, item) {
        if (err) {
          throw err;
        }
        if (item) {
          return store.zrem('request-queue', req.params.id, function(err) {
            if (err) {
              throw err;
            }
            return store.del('request:' + req.params.id, function(err) {
              if (err) {
                throw err;
              }
              return res.send({
                status: 'ok',
                statusText: 'The item has been removed from the queue'
              });
            });
          });
        } else {
          return do404(res);
        }
      });
    });
  };

}).call(this);
